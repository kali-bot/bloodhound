Source: bloodhound
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               libatk1.0-dev,
               libpangocairo-1.0-0,
               libgdk-pixbuf2.0-dev,
               libcairo2-dev,
               libpango1.0-dev,
               libfreetype6-dev,
               libfontconfig1-dev,
               libdbus-1-dev,
               libx11-xcb-dev,
               libxcb1-dev,
               libxi-dev,
               libxcursor-dev,
               libxdamage-dev,
               libxrandr-dev,
               libxcomposite-dev,
               libxext-dev,
               libxfixes-dev,
               libxrender-dev,
               libx11-dev,
               libxtst-dev,
               libxss-dev,
               libgconf2-dev,
               libnss3-dev,
               libnspr4-dev,
               libasound2-dev,
               libcups2-dev,
               libgtk2.0-dev
Standards-Version: 4.4.0
Homepage: https://github.com/BloodHoundAD/BloodHound
Vcs-Git: https://gitlab.com/kalilinux/packages/bloodhound.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/bloodhound

Package: bloodhound
Architecture: amd64 armhf arm64
Depends: ${shlibs:Depends}, ${misc:Depends}, neo4j
Description: Six Degrees of Domain Admin
 This package contains BloodHound, a single page Javascript web application.
 BloodHound uses graph theory to reveal the hidden and often unintended
 relationships within an Active Directory environment. Attackers can use
 BloodHound to easily identify highly complex attack paths that would otherwise
 be impossible to quickly identify. Defenders can use BloodHound to identify and
 eliminate those same attack paths. Both blue and red teams can use BloodHound
 to easily gain a deeper understanding of privilege relationships in an Active
 Directory environment.
